package com.irfanirawansukirman.awesomeapp

import android.app.Application
import com.facebook.stetho.Stetho
import com.irfanirawansukirman.gallery.di.DaggerImageComponent
import com.irfanirawansukirman.gallery.di.ImageComponent
import com.irfanirawansukirman.gallery.di.ImageComponentProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
class AwesomeApp : Application(), ImageComponentProvider {

    override fun onCreate() {
        super.onCreate()
        initStetho()
    }

    private fun initStetho() {
        if (BuildConfig.DEBUG) Stetho.initializeWithDefaults(this)
    }

    override fun getImageComponent(): ImageComponent {
        return DaggerImageComponent
            .builder()
            .application(this)
            .build()
    }
}