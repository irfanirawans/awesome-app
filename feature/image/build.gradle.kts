plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

android {
    compileSdk = Android.compileSdk

    defaultConfig {
        minSdk = Android.minSdk
        targetSdk = Android.targetSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
    lintOptions {
        isAbortOnError = false
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(project(":cache"))
    implementation(project(":remote"))
    implementation(project(":core"))

    kapt(App.daggerCompiler)

    testImplementation(App.junit)
    testImplementation(App.mockk)
    testImplementation(App.coreTesting)
    testImplementation(App.coroutinesTest)
    testImplementation(App.googleTruth)
    testImplementation(App.turbine)
    testImplementation(App.testRunner)

    androidTestImplementation(App.espressoCore)
    androidTestImplementation(App.testRules)
    androidTestImplementation(App.junitExt)
    androidTestImplementation(App.mockWebServer)
}