package com.irfanirawansukirman.gallery.feature.image.data

import androidx.test.filters.SmallTest
import app.cash.turbine.test
import com.google.common.truth.Truth
import com.irfanirawansukirman.gallery.data.ImageAppRepositoryImpl
import com.irfanirawansukirman.gallery.data.cache.repository.ImageCacheRepositoryImpl
import com.irfanirawansukirman.gallery.data.mapper.ImageDomainDataMapper
import com.irfanirawansukirman.gallery.data.remote.repository.ImageRemoteRepositoryImpl
import com.irfanirawansukirman.gallery.util.DataGenerator
import com.irfanirawansukirman.remote.util.Resource
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
@SmallTest
class ImageAppRepositoryImplTest {

    @MockK
    private lateinit var remoteDataSource: ImageRemoteRepositoryImpl

    @MockK
    private lateinit var localDataSource: ImageCacheRepositoryImpl

    private val imageDomainDataMapper = ImageDomainDataMapper()

    private lateinit var repository: ImageAppRepositoryImpl

    @Before
    fun `setup depends`() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        repository = ImageAppRepositoryImpl(
            remoteDataSource,
            localDataSource,
            imageDomainDataMapper
        )
    }

    @Test
    fun `get remote images is success`() = runBlockingTest {
        val photos = DataGenerator.generateDataModelPhotos()

        coEvery { remoteDataSource.getImages() } returns photos

        val response = repository.getRemoteImages()
        response.test {
            val expected = expectItem()
            val expectedData = (expected as Resource.Success).data
            Truth.assertThat(expected).isInstanceOf(Resource.Success::class.java)
            Truth.assertThat(expectedData)
                .containsAtLeastElementsIn(imageDomainDataMapper.fromList(photos))
            expectComplete()
        }

        coVerify { remoteDataSource.getImages() }
    }

    @Test
    fun `get remote images is failed`() = runBlockingTest {
        coEvery { remoteDataSource.getImages() } throws Exception()

        val response = repository.getRemoteImages()
        response.test {
            Truth.assertThat(expectItem()).isInstanceOf(Resource.Error::class.java)
            expectComplete()
        }

        coVerify { remoteDataSource.getImages() }
    }

    @Test
    fun `get cache images is success`() = runBlockingTest {
        val photos = DataGenerator.generateDataModelPhotos()

        coEvery { localDataSource.getCacheAllImages() } returns photos

        val response = repository.getCacheImages()
        response.test {
            val expected = expectItem()
            val expectedData = (expected as Resource.Success).data
            Truth.assertThat(expected).isInstanceOf(Resource.Success::class.java)
            Truth.assertThat(expectedData)
                .containsAtLeastElementsIn(imageDomainDataMapper.fromList(photos))
            expectComplete()
        }

        coVerify { localDataSource.getCacheAllImages() }
    }

    @Test
    fun `get cache images is failed`() = runBlockingTest {
        coEvery { localDataSource.getCacheAllImages() } throws Exception()

        val response = repository.getCacheImages()
        response.test {
            Truth.assertThat(expectItem()).isInstanceOf(Resource.Error::class.java)
            expectComplete()
        }

        coVerify { localDataSource.getCacheAllImages() }
    }

    @Test
    fun `insert image is failed`() = runBlockingTest {
        val entity = DataGenerator.generateDataModelPhoto()

        coEvery { localDataSource.insertImages(entity) } throws Exception()

        val response = repository.insertImage(entity)
        response.test {
            Truth.assertThat(expectItem()).isInstanceOf(Resource.Error::class.java)
            expectComplete()
        }

        coVerify { localDataSource.insertImages(entity) }
    }

    @After
    fun `clear all`() {
        clearAllMocks()
    }
}