package com.irfanirawansukirman.gallery.feature.image.data

import androidx.test.filters.SmallTest
import com.google.common.truth.Truth
import com.irfanirawansukirman.cache.dao.ImageDao
import com.irfanirawansukirman.gallery.data.cache.mapper.ImageCacheMapper
import com.irfanirawansukirman.gallery.data.cache.repository.ImageCacheRepository
import com.irfanirawansukirman.gallery.data.cache.repository.ImageCacheRepositoryImpl
import com.irfanirawansukirman.gallery.util.DataGenerator
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
@SmallTest
class ImageCacheRepositoryImplTest {

    @MockK
    private lateinit var imageDao: ImageDao

    private lateinit var repositoryImpl: ImageCacheRepository

    private val imageCacheMapper = ImageCacheMapper()

    @Before
    fun `setup depends`() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        repositoryImpl = ImageCacheRepositoryImpl(imageDao, imageCacheMapper)
    }

    @Test
    fun `get cache photos is success`() = runBlockingTest {
        val photos = DataGenerator.generateCachePhotos()
        val expected = imageCacheMapper.fromList(photos)

        coEvery { imageDao.getAllImages() } returns photos

        val response = repositoryImpl.getCacheAllImages()

        coVerify { imageDao.getAllImages() }

        Truth.assertThat(response).containsAtLeastElementsIn(expected)
    }

    @Test(expected = Exception::class)
    fun `get cache photos is failed`() = runBlockingTest {
        coEvery { imageDao.getAllImages() } throws Exception()

        repositoryImpl.getCacheAllImages()

        coVerify { imageDao.getAllImages() }
    }

    @After
    fun `clear all`() {
        clearAllMocks()
    }
}