package com.irfanirawansukirman.gallery.util

import com.irfanirawansukirman.cache.entity.ImageEnt
import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import com.irfanirawansukirman.gallery.domain.entity.ImageDomainEntity
import com.irfanirawansukirman.remote.data.response.Photo
import com.irfanirawansukirman.remote.data.response.Src

object DataGenerator {

    fun generateRemotePhotos(): List<Photo> {
        val src = Src(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        )
        val photo = Photo(
            "",
            0,
            0,
            false,
            "",
            0,
            "",
            src,
            "",
            0
        )

        return listOf(photo)
    }

    fun generateCachePhotos(): List<ImageEnt> {
        val entity = ImageEnt(
            0,
            "",
            ""
        )

        return listOf(entity)
    }

    fun generateDataModelPhotos(): List<ImageDataModel> {
        val item = ImageDataModel(
            0,
            "",
            ""
        )

        return listOf(item)
    }

    fun generateDataModelPhoto(): ImageDataModel {
        return ImageDataModel(
            0,
            "",
            ""
        )
    }

    fun generateDomainPhotos(): List<ImageDomainEntity> {
        val entity = ImageDomainEntity(
            0,
            "",
            ""
        )

        return listOf(entity)
    }
}