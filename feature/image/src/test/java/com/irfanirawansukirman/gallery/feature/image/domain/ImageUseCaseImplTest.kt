package com.irfanirawansukirman.gallery.feature.image.domain

import androidx.test.filters.SmallTest
import app.cash.turbine.test
import com.google.common.truth.Truth
import com.irfanirawansukirman.gallery.data.ImageAppRepositoryImpl
import com.irfanirawansukirman.gallery.domain.ImageUseCaseImpl
import com.irfanirawansukirman.gallery.util.DataGenerator
import com.irfanirawansukirman.gallery.util.MainCoroutinesRule
import com.irfanirawansukirman.remote.util.Resource
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
@SmallTest
class ImageUseCaseImplTest {

    @get:Rule
    val mainCoroutineRule = MainCoroutinesRule()

    @MockK
    private lateinit var appRepository: ImageAppRepositoryImpl

    private lateinit var useCase: ImageUseCaseImpl

    @Before
    fun `setup depends`() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        useCase = ImageUseCaseImpl(appRepository)
    }

    @Test
    fun `get remote images is success`() = runBlockingTest {
        val photos = DataGenerator.generateDomainPhotos()
        val successResponseFlow = flowOf(Resource.Success(photos))

        coEvery { appRepository.getRemoteImages() } returns successResponseFlow

        val response = useCase.getRemoteImages()
        response.test {
            val expected = expectItem()
            val expectedData = (expected as Resource.Success).data
            Truth.assertThat(expected).isInstanceOf(Resource.Success::class.java)
            Truth.assertThat(expectedData).containsAtLeastElementsIn(photos)
            expectComplete()
        }

        coVerify { appRepository.getRemoteImages() }
    }

    @Test
    fun `get remote images is failed`() = runBlockingTest {
        val failedResponseFlow = flowOf(Resource.Error(Exception()))

        coEvery { appRepository.getRemoteImages() } returns failedResponseFlow

        val response = useCase.getRemoteImages()
        response.test {
            Truth.assertThat(expectItem()).isInstanceOf(Resource.Error::class.java)
            expectComplete()
        }

        coEvery { appRepository.getRemoteImages() }
    }

    @Test
    fun `get cache movies popular is success`() = runBlockingTest {
        val photos = DataGenerator.generateDomainPhotos()
        val successResponseFlow = flowOf(Resource.Success(photos))

        coEvery { appRepository.getCacheImages() } returns successResponseFlow

        val response = useCase.getCacheImages()
        response.test {
            val expected = expectItem()
            val expectedData = (expected as Resource.Success).data
            Truth.assertThat(expected).isInstanceOf(Resource.Success::class.java)
            Truth.assertThat(expectedData).containsAtLeastElementsIn(photos)
            expectComplete()
        }

        coVerify { appRepository.getCacheImages() }
    }

    @Test
    fun `get cache images is failed`() = runBlockingTest {
        val failedResponseFlow = flowOf(Resource.Error(Exception()))

        coEvery { appRepository.getCacheImages() } returns failedResponseFlow

        val response = useCase.getCacheImages()
        response.test {
            Truth.assertThat(expectItem()).isInstanceOf(Resource.Error::class.java)
            expectComplete()
        }

        coEvery { appRepository.getCacheImages() }
    }

    @Test
    fun `insert image is success`() = runBlockingTest {
        val entity = DataGenerator.generateDataModelPhoto()
        val successResponseFlow = flowOf(Resource.Success("Success"))

        coEvery { appRepository.insertImage(entity) } returns successResponseFlow

        val response = useCase.insertImage(entity)
        response.test {
            val expected = expectItem()
            val expectedData = (expected as Resource.Success).data
            Truth.assertThat(expected).isInstanceOf(Resource.Success::class.java)
            Truth.assertThat(expectedData).isEqualTo("Success")
            expectComplete()
        }

        coVerify { appRepository.insertImage(entity) }
    }

    @Test
    fun `insert image is failed`() = runBlockingTest {
        val entity = DataGenerator.generateDataModelPhoto()
        val failedResponseFlow = flowOf(Resource.Error(Exception("Failed")))

        coEvery { appRepository.insertImage(entity) } returns failedResponseFlow

        val response = useCase.insertImage(entity)
        response.test {
            val expected = expectItem()
            val expectedData = (expected as Resource.Error).exception.message
            Truth.assertThat(expected).isInstanceOf(Resource.Error::class.java)
            Truth.assertThat(expectedData).isEqualTo("Failed")
            expectComplete()
        }

        coVerify { appRepository.insertImage(entity) }
    }

    @After
    fun `clear all`() {
        clearAllMocks()
    }
}