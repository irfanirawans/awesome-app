package com.irfanirawansukirman.gallery.feature.image.data

import androidx.test.filters.SmallTest
import com.google.common.truth.Truth
import com.irfanirawansukirman.gallery.data.remote.mapper.ImageNetworkMapper
import com.irfanirawansukirman.gallery.data.remote.repository.ImageRemoteRepository
import com.irfanirawansukirman.gallery.data.remote.repository.ImageRemoteRepositoryImpl
import com.irfanirawansukirman.gallery.util.DataGenerator
import com.irfanirawansukirman.remote.data.service.ImageService
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Test

@ExperimentalCoroutinesApi
@SmallTest
class ImageRemoteRepositoryImplTest {

    @MockK
    private lateinit var imageService: ImageService

    private lateinit var repositoryImpl: ImageRemoteRepository

    private val imageNetworkMapper = ImageNetworkMapper()

    @Before
    fun `setup depends`() {
        MockKAnnotations.init(this, relaxUnitFun = true)

        repositoryImpl = ImageRemoteRepositoryImpl(imageService, imageNetworkMapper)
    }

    @Test
    fun `get images is success`() = runBlockingTest {
        val photos = DataGenerator.generateRemotePhotos()

        coEvery { imageService.getImages("random", 10).photos } returns photos

        val response = repositoryImpl.getImages()

        coVerify { imageService.getImages("random", 10) }

        val expected = imageNetworkMapper.fromList(photos)

        Truth.assertThat(response).containsExactlyElementsIn(expected)
    }

    @Test(expected = Exception::class)
    fun `get images is failed`() = runBlockingTest {
        coEvery { imageService.getImages("random", 10).photos } throws Exception()

        repositoryImpl.getImages()

        coVerify { imageService.getImages("random", 10) }
    }

    @After
    fun `clear all`() {
        clearAllMocks()
    }
}