package com.irfanirawansukirman.gallery.feature.image.presentation

import androidx.test.filters.SmallTest
import app.cash.turbine.test
import com.google.common.truth.Truth
import com.irfanirawansukirman.gallery.domain.ImageUseCaseImpl
import com.irfanirawansukirman.gallery.presentation.ui.list.ImageContract
import com.irfanirawansukirman.gallery.presentation.ui.list.ImageViewModel
import com.irfanirawansukirman.gallery.presentation.mapper.ImageDomainUiMapper
import com.irfanirawansukirman.gallery.util.DataGenerator
import com.irfanirawansukirman.gallery.util.MainCoroutinesRule
import com.irfanirawansukirman.remote.util.Resource
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.time.ExperimentalTime

@ExperimentalTime
@ExperimentalCoroutinesApi
@SmallTest
class ImageViewModelTest {

    @get:Rule
    var coroutinesRule = MainCoroutinesRule()

    @MockK
    private lateinit var useCase: ImageUseCaseImpl

    private val mapper = ImageDomainUiMapper()

    private lateinit var viewModel: ImageViewModel

    @Before
    fun `setup depends`() {
        MockKAnnotations.init(this, relaxUnitFun = true)
        viewModel = ImageViewModel(useCase, mapper)
    }

    @Test
    fun `get remote images is success`() = runBlockingTest {
        val photos = DataGenerator.generateDomainPhotos()
        val successResponseFlow = flowOf(Resource.Success(photos))

        coEvery { useCase.getRemoteImages() } returns successResponseFlow

        viewModel.uiState.test {
            viewModel.setEvent(ImageContract.ImageEvent.OnGetRemoteImages)

            Truth.assertThat(expectItem()).isEqualTo(
                ImageContract.State(
                    state = ImageContract.ImageState.Idle
                )
            )

            Truth.assertThat(expectItem()).isEqualTo(
                ImageContract.State(
                    state = ImageContract.ImageState.Loading
                )
            )

            val expected = expectItem()
            val expectedPhotos =
                (expected.state as ImageContract.ImageState.SuccessRemoteGetImages).images
            Truth.assertThat(expected).isEqualTo(
                ImageContract.State(
                    state = ImageContract.ImageState.SuccessRemoteGetImages(
                        mapper.fromList(photos)
                    )
                )
            )
            Truth.assertThat(expectedPhotos).containsExactlyElementsIn(mapper.fromList(photos))
            cancelAndIgnoreRemainingEvents()
        }

        coVerify { useCase.getRemoteImages() }
    }

    @Test
    fun `get remote images is failed`() = runBlockingTest {
        val failedResponseFlow = flowOf(Resource.Error(Exception("Error")))

        coEvery { useCase.getRemoteImages() } returns failedResponseFlow

        viewModel.uiState.test {
            viewModel.setEvent(ImageContract.ImageEvent.OnGetRemoteImages)

            Truth.assertThat(expectItem()).isEqualTo(
                ImageContract.State(
                    state = ImageContract.ImageState.Idle
                )
            )

            Truth.assertThat(expectItem()).isEqualTo(
                ImageContract.State(
                    state = ImageContract.ImageState.Loading
                )
            )

            cancelAndIgnoreRemainingEvents()
        }

        viewModel.effect.test {
            val expected = expectItem()
            val expectedMessage = (expected as ImageContract.ImageEffect.Error).message
            Truth.assertThat(expected).isEqualTo(
                ImageContract.ImageEffect.Error("Error")
            )
            Truth.assertThat(expectedMessage).isEqualTo("Error")

            cancelAndIgnoreRemainingEvents()
        }
    }

    @After
    fun `clear all`() {
        clearAllMocks()
    }
}