package com.irfanirawansukirman.gallery.data.cache.repository

import com.irfanirawansukirman.cache.dao.ImageDao
import com.irfanirawansukirman.gallery.data.cache.mapper.ImageCacheMapper
import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import javax.inject.Inject

class ImageCacheRepositoryImpl @Inject constructor(
    private val imageDao: ImageDao,
    private val imageCacheMapper: ImageCacheMapper
) : ImageCacheRepository {

    override suspend fun insertImages(imageDataModel: ImageDataModel) {
        val entity = imageCacheMapper.after(imageDataModel)
        imageDao.insertObject(entity)
    }

    override suspend fun getCacheAllImages(): List<ImageDataModel> {
        val images = imageDao.getAllImages()
        return imageCacheMapper.fromList(images)
    }
}