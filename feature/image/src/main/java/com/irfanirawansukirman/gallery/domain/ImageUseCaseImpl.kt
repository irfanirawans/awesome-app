package com.irfanirawansukirman.gallery.domain

import com.irfanirawansukirman.gallery.data.ImageAppRepository
import com.irfanirawansukirman.gallery.data.ImageAppRepositoryImpl
import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import com.irfanirawansukirman.gallery.domain.entity.ImageDomainEntity
import com.irfanirawansukirman.remote.util.Resource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ImageUseCaseImpl @Inject constructor(
    private val repository: ImageAppRepositoryImpl
) : ImageAppRepository {

    override suspend fun getRemoteImages(): Flow<Resource<List<ImageDomainEntity>>> {
        return repository.getRemoteImages()
    }

    override suspend fun insertImage(imageDataModel: ImageDataModel): Flow<Resource<String>> {
        return repository.insertImage(imageDataModel)
    }

    override suspend fun getCacheImages(): Flow<Resource<List<ImageDomainEntity>>> {
        return repository.getCacheImages()
    }
}