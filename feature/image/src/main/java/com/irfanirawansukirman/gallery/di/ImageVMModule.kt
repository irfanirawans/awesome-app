package com.irfanirawansukirman.gallery.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.irfanirawansukirman.core.ViewModelFactory
import com.irfanirawansukirman.core.ViewModelKey
import com.irfanirawansukirman.gallery.presentation.ui.list.ImageViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module
abstract class ImageVMModule {

    @Binds
    internal abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ImageViewModel::class)
    internal abstract fun bindImageViewModel(imageViewModel: ImageViewModel): ViewModel
}