package com.irfanirawansukirman.gallery.data

import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import com.irfanirawansukirman.gallery.domain.entity.ImageDomainEntity
import com.irfanirawansukirman.remote.util.Resource
import kotlinx.coroutines.flow.Flow

interface ImageAppRepository {

    suspend fun getRemoteImages(): Flow<Resource<List<ImageDomainEntity>>>

    suspend fun insertImage(imageDataModel: ImageDataModel): Flow<Resource<String>>

    suspend fun getCacheImages(): Flow<Resource<List<ImageDomainEntity>>>
}