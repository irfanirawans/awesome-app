package com.irfanirawansukirman.gallery.data

import com.irfanirawansukirman.gallery.data.cache.repository.ImageCacheRepositoryImpl
import com.irfanirawansukirman.gallery.data.mapper.ImageDomainDataMapper
import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import com.irfanirawansukirman.gallery.data.remote.repository.ImageRemoteRepositoryImpl
import com.irfanirawansukirman.gallery.domain.entity.ImageDomainEntity
import com.irfanirawansukirman.remote.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class ImageAppRepositoryImpl @Inject constructor(
    private val remoteDataSource: ImageRemoteRepositoryImpl,
    private val cacheDataSource: ImageCacheRepositoryImpl,
    private val domainDataMapper: ImageDomainDataMapper
) : ImageAppRepository {

    override suspend fun getRemoteImages(): Flow<Resource<List<ImageDomainEntity>>> {
        return flow {
            try {
                val images = remoteDataSource.getImages()
                emit(Resource.Success(domainDataMapper.fromList(images)))
            } catch (e: Exception) {
                emit(Resource.Error(e))
            }
        }
    }

    override suspend fun insertImage(imageDataModel: ImageDataModel): Flow<Resource<String>> {
        return flow {
            try {
                cacheDataSource.insertImages(imageDataModel)
                emit(Resource.Success(if (true) "Success" else "Failed"))
            } catch (e: Exception) {
                emit(Resource.Error(e))
            }
        }
    }

    override suspend fun getCacheImages(): Flow<Resource<List<ImageDomainEntity>>> {
        return flow {
            try {
                val images = cacheDataSource.getCacheAllImages()
                emit(Resource.Success(domainDataMapper.fromList(images)))
            } catch (e: Exception) {
                emit(Resource.Error(e))
            }
        }
    }
}