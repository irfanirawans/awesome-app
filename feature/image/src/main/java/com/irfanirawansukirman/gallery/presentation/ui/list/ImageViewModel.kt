package com.irfanirawansukirman.gallery.presentation.ui.list

import androidx.lifecycle.viewModelScope
import com.irfanirawansukirman.core.base.BaseViewModel
import com.irfanirawansukirman.gallery.domain.ImageUseCaseImpl
import com.irfanirawansukirman.gallery.presentation.mapper.ImageDomainUiMapper
import com.irfanirawansukirman.remote.util.Resource
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class ImageViewModel @Inject constructor(
    private val useCase: ImageUseCaseImpl,
    private val imageDomainUiMapper: ImageDomainUiMapper
) : BaseViewModel<ImageContract.ImageEvent, ImageContract.State, ImageContract.ImageEffect>(),
    ImageContract.Job {

    override fun createInitialState(): ImageContract.State {
        return ImageContract.State(ImageContract.ImageState.Idle)
    }

    override fun handleEvent(event: ImageContract.ImageEvent) {
        when (event) {
            is ImageContract.ImageEvent.OnGetRemoteImages -> getRemoteImages()
        }
    }

    override fun getRemoteImages() {
        viewModelScope.launch {
            useCase.getRemoteImages()
                .onStart { emit(Resource.Loading) }
                .collect {
                    when (it) {
                        is Resource.Loading -> setState { copy(state = ImageContract.ImageState.Loading) }
                        is Resource.Empty -> setState { copy(state = ImageContract.ImageState.Idle) }
                        is Resource.Success -> {
                            val photos = it.data ?: emptyList()
                            setState {
                                copy(
                                    state = ImageContract.ImageState.SuccessRemoteGetImages(
                                        imageDomainUiMapper.fromList(photos)
                                    )
                                )
                            }
                        }
                        is Resource.Error -> setEffect {
                            ImageContract.ImageEffect.Error(
                                it.exception.message ?: "Error"
                            )
                        }
                    }
                }
        }
    }
}