package com.irfanirawansukirman.gallery.data.remote.repository

import com.irfanirawansukirman.gallery.data.model.ImageDataModel

interface ImageRemoteRepository {

    suspend fun getImages(): List<ImageDataModel>
}