package com.irfanirawansukirman.gallery.di

import com.irfanirawansukirman.gallery.data.cache.mapper.ImageCacheMapper
import com.irfanirawansukirman.gallery.data.mapper.ImageDomainDataMapper
import com.irfanirawansukirman.gallery.data.remote.mapper.ImageNetworkMapper
import com.irfanirawansukirman.gallery.presentation.mapper.ImageDomainUiMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ImageMapperModule {

    @Singleton
    @Provides
    fun provideImageNetworkMapper() = ImageNetworkMapper()

    @Singleton
    @Provides
    fun provideImageCacheMapper() = ImageCacheMapper()

    @Singleton
    @Provides
    fun provideImageDataDomainMapper() = ImageDomainDataMapper()

    @Singleton
    @Provides
    fun provideImageDomainUiMapper() = ImageDomainUiMapper()
}