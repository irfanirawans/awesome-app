package com.irfanirawansukirman.gallery.data.remote.repository

import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import com.irfanirawansukirman.gallery.data.remote.mapper.ImageNetworkMapper
import com.irfanirawansukirman.remote.data.service.ImageService
import javax.inject.Inject

class ImageRemoteRepositoryImpl @Inject constructor(
    private val imageService: ImageService,
    private val imageNetworkMapper: ImageNetworkMapper
) : ImageRemoteRepository {

    override suspend fun getImages(): List<ImageDataModel> {
        val response = imageService.getImages()
        return imageNetworkMapper.fromList(response.photos)
    }
}