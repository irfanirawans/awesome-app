package com.irfanirawansukirman.gallery.di

import com.irfanirawansukirman.cache.dao.ImageDao
import com.irfanirawansukirman.cache.di.CacheModule
import com.irfanirawansukirman.core.base.BaseModule
import com.irfanirawansukirman.gallery.data.ImageAppRepositoryImpl
import com.irfanirawansukirman.gallery.data.cache.mapper.ImageCacheMapper
import com.irfanirawansukirman.gallery.data.cache.repository.ImageCacheRepositoryImpl
import com.irfanirawansukirman.gallery.data.mapper.ImageDomainDataMapper
import com.irfanirawansukirman.gallery.data.remote.mapper.ImageNetworkMapper
import com.irfanirawansukirman.gallery.data.remote.repository.ImageRemoteRepositoryImpl
import com.irfanirawansukirman.remote.data.service.ImageService
import com.irfanirawansukirman.remote.di.RemoteModule
import com.irfanirawansukirman.remote.factory.ApiFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [BaseModule::class, RemoteModule::class, CacheModule::class])
class ImageDataModule {

    @Singleton
    @Provides
    fun provideImageService(retrofit: Retrofit): ImageService = ApiFactory.getService(retrofit)

    @Singleton
    @Provides
    fun provideImageRemoteRepositoryImpl(
        imageService: ImageService,
        imageNetworkMapper: ImageNetworkMapper
    ): ImageRemoteRepositoryImpl {
        return ImageRemoteRepositoryImpl(imageService, imageNetworkMapper)
    }

    @Singleton
    @Provides
    fun provideImageCacheRepositoryImpl(
        imageDao: ImageDao,
        imageCacheMapper: ImageCacheMapper
    ): ImageCacheRepositoryImpl {
        return ImageCacheRepositoryImpl(imageDao, imageCacheMapper)
    }

    @Singleton
    @Provides
    fun provideImageAppRepositoryImpl(
        imageRemoteRepositoryImpl: ImageRemoteRepositoryImpl,
        imageCacheRepositoryImpl: ImageCacheRepositoryImpl,
        imageDomainDataMapper: ImageDomainDataMapper
    ): ImageAppRepositoryImpl {
        return ImageAppRepositoryImpl(
            imageRemoteRepositoryImpl,
            imageCacheRepositoryImpl,
            imageDomainDataMapper
        )
    }
}