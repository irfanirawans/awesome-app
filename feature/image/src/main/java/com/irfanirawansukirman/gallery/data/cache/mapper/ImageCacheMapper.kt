package com.irfanirawansukirman.gallery.data.cache.mapper

import com.irfanirawansukirman.cache.entity.ImageEnt
import com.irfanirawansukirman.core.Mapper
import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import javax.inject.Inject

class ImageCacheMapper @Inject constructor() : Mapper<ImageEnt, ImageDataModel> {

    override fun before(previous: ImageEnt?): ImageDataModel {
        return ImageDataModel(
            previous?.id,
            previous?.photographer,
            previous?.image
        )
    }

    override fun after(next: ImageDataModel?): ImageEnt {
        return ImageEnt(
            next?.id,
            next?.photographer,
            next?.image
        )
    }
}