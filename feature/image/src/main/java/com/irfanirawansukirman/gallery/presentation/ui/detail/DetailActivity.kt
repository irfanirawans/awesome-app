package com.irfanirawansukirman.gallery.presentation.ui.detail

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.irfanirawansukirman.core.ext.load
import com.irfanirawansukirman.core.util.IMAGE
import com.irfanirawansukirman.gallery.databinding.DetailActivityBinding

class DetailActivity : AppCompatActivity() {

    private var binding: DetailActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DetailActivityBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        showImage()
    }

    private fun showImage() {
        val imageUrl = intent?.getStringExtra(IMAGE) ?: ""
        binding?.ivImage?.load(imageUrl) {}
    }
}