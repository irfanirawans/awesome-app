package com.irfanirawansukirman.gallery.presentation.ui.list

import com.irfanirawansukirman.core.UiEffect
import com.irfanirawansukirman.core.UiEvent
import com.irfanirawansukirman.core.UiState
import com.irfanirawansukirman.core.base.BaseJob
import com.irfanirawansukirman.gallery.presentation.model.ImageUiModel

object ImageContract {

    interface Job: BaseJob {

        fun getRemoteImages() {}
    }

    sealed class ImageEvent : UiEvent {
        object OnGetRemoteImages : ImageEvent()
    }

    sealed class ImageState {
        object Idle : ImageState()
        object Loading : ImageState()
        data class SuccessRemoteGetImages(val images: List<ImageUiModel>) : ImageState()
    }

    sealed class ImageEffect : UiEffect {
        data class Error(val message: String) : ImageEffect()
    }

    data class State(val state: ImageState) : UiState
}