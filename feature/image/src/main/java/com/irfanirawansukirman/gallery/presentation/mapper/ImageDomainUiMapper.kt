package com.irfanirawansukirman.gallery.presentation.mapper

import com.irfanirawansukirman.core.Mapper
import com.irfanirawansukirman.gallery.domain.entity.ImageDomainEntity
import com.irfanirawansukirman.gallery.presentation.model.ImageUiModel
import javax.inject.Inject

class ImageDomainUiMapper @Inject constructor() : Mapper<ImageDomainEntity, ImageUiModel> {

    override fun before(previous: ImageDomainEntity?): ImageUiModel {
        return ImageUiModel(
            previous?.id,
            previous?.photographer,
            previous?.image
        )
    }

    override fun after(next: ImageUiModel?): ImageDomainEntity {
        return ImageDomainEntity(
            next?.id,
            next?.photographer,
            next?.image
        )
    }
}