package com.irfanirawansukirman.gallery.data.cache.repository

import com.irfanirawansukirman.gallery.data.model.ImageDataModel

interface ImageCacheRepository {

    suspend fun insertImages(imageDataModel: ImageDataModel)

    suspend fun getCacheAllImages(): List<ImageDataModel>
}