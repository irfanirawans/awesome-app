package com.irfanirawansukirman.gallery.di

import android.app.Application
import com.irfanirawansukirman.gallery.presentation.ui.list.ImageActivity
import dagger.BindsInstance
import dagger.Component
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@Singleton
@Component(
    modules = [
        ImageDomainModule::class,
        ImageVMModule::class,
        ImageMapperModule::class
    ]
)
interface ImageComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ImageComponent
    }

    fun inject(activity: ImageActivity)
}