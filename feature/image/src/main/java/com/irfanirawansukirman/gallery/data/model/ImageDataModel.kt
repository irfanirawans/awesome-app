package com.irfanirawansukirman.gallery.data.model

data class ImageDataModel(
    val id: Int?,
    val photographer: String?,
    val image: String?
)