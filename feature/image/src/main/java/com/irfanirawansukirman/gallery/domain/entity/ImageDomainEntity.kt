package com.irfanirawansukirman.gallery.domain.entity

data class ImageDomainEntity(
    val id: Int?,
    val photographer: String?,
    val image: String?
)