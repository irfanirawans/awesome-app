package com.irfanirawansukirman.gallery.di

import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
interface ImageComponentProvider {

    fun getImageComponent(): ImageComponent
}