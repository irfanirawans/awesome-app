package com.irfanirawansukirman.gallery.di

import com.irfanirawansukirman.gallery.data.ImageAppRepositoryImpl
import com.irfanirawansukirman.gallery.domain.ImageUseCaseImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ImageDataModule::class])
class ImageDomainModule {

    @Singleton
    @Provides
    fun provideImageUseCaseImpl(imageAppRepositoryImpl: ImageAppRepositoryImpl): ImageUseCaseImpl {
        return ImageUseCaseImpl(imageAppRepositoryImpl)
    }
}