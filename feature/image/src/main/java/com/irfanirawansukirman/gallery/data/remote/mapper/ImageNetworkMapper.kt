package com.irfanirawansukirman.gallery.data.remote.mapper

import com.irfanirawansukirman.core.Mapper
import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import com.irfanirawansukirman.remote.data.response.Photo
import com.irfanirawansukirman.remote.data.response.Src
import javax.inject.Inject

class ImageNetworkMapper @Inject constructor() : Mapper<Photo, ImageDataModel> {

    override fun before(previous: Photo?): ImageDataModel {
        return ImageDataModel(
            previous?.id,
            previous?.photographer,
            previous?.src?.large
        )
    }

    override fun after(next: ImageDataModel?): Photo {
        val src = Src(
            "",
            "",
            "",
            next?.image,
            "",
            "",
            "",
            ""
        )
        return Photo(
            "",
            0,
            0,
            false,
            next?.photographer,
            0,
            "",
            src,
            "",
            0
        )
    }
}