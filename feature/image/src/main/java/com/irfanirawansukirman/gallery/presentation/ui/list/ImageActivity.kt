package com.irfanirawansukirman.gallery.presentation.ui.list

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.irfanirawansukirman.core.util.IMAGE
import com.irfanirawansukirman.core.util.POS_START
import com.irfanirawansukirman.core.util.SPAN_1
import com.irfanirawansukirman.core.util.SPAN_2
import com.irfanirawansukirman.core.util.TRANSITION
import com.irfanirawansukirman.gallery.R
import com.irfanirawansukirman.gallery.databinding.ImageActivityBinding
import com.irfanirawansukirman.gallery.di.DaggerImageComponent
import com.irfanirawansukirman.gallery.di.ImageComponentProvider
import com.irfanirawansukirman.gallery.presentation.model.ImageUiModel
import com.irfanirawansukirman.gallery.presentation.ui.detail.DetailActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

@ExperimentalCoroutinesApi
class ImageActivity : AppCompatActivity(), ImageContract.Job, ImageAdapter.ImageListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ImageViewModel> { viewModelFactory }

    private lateinit var adapter: ImageAdapter
    private lateinit var layoutManager: GridLayoutManager

    private var binding: ImageActivityBinding? = null

    override fun onDestroy() {
        binding = null
        super.onDestroy()
    }

    override fun initInjector() {
        (application as ImageComponentProvider)
            .getImageComponent()
            .inject(this)
    }

    override fun bindView() {
        if (binding == null) binding = ImageActivityBinding.inflate(layoutInflater)
        setContentView(binding?.root)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        initInjector()
        super.onCreate(savedInstanceState)
        bindView()

        setSupportActionBar(binding?.toolbar)

        initAdapter()
        initImageList()
        initObservers()

        getRemoteImages()
    }

    override fun getRemoteImages() {
        if (viewModel.currentState.state is ImageContract.ImageState.Idle) {
            viewModel.setEvent(ImageContract.ImageEvent.OnGetRemoteImages)
        }
    }

    override fun onClickImage(item: ImageUiModel, view: ImageView) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra(IMAGE, item.image)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, TRANSITION)
        startActivity(intent, options.toBundle())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.image, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_switch) {
            switchLayout()
            switchIcon(item)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun switchIcon(item: MenuItem) {
        item.icon = ContextCompat.getDrawable(
            this,
            if (layoutManager.spanCount == SPAN_2) R.drawable.ic_toggle_on else R.drawable.ic_toggle_off
        )
    }

    private fun switchLayout() {
        layoutManager.spanCount = if (layoutManager.spanCount == SPAN_1) SPAN_2 else SPAN_1
        adapter.notifyItemRangeChanged(POS_START, adapter.itemCount)
    }

    private fun initAdapter() {
        if (!::adapter.isInitialized) {
            adapter = ImageAdapter(this)
        }
    }

    private fun initImageList() {
        if (!::layoutManager.isInitialized) {
            layoutManager = GridLayoutManager(this, 1)
        }

        binding?.rvImage?.apply {
            layoutManager = this@ImageActivity.layoutManager
            setHasFixedSize(true)
            adapter = this@ImageActivity.adapter
        }
    }

    private fun initObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                when (it.state) {
                    is ImageContract.ImageState.Idle -> hideProgress()
                    is ImageContract.ImageState.Loading -> showProgress()
                    is ImageContract.ImageState.SuccessRemoteGetImages -> {
                        hideProgress()
                        showImages(it.state.images)
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.effect.collect {
                when (it) {
                    is ImageContract.ImageEffect.Error -> hideProgress()
                }
            }
        }
    }

    private fun hideProgress() {
        binding?.progress?.isVisible = false
    }

    private fun showProgress() {
        binding?.progress?.isVisible = true
    }

    private fun showImages(images: List<ImageUiModel>) {
        adapter.addAllPhotos(images)
    }
}