package com.irfanirawansukirman.gallery.presentation.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.irfanirawansukirman.core.ext.load
import com.irfanirawansukirman.gallery.databinding.ImageItemBinding
import com.irfanirawansukirman.gallery.presentation.model.ImageUiModel

class ImageAdapter(private val imageListener: ImageListener) :
    RecyclerView.Adapter<ImageAdapter.ItemHolder>() {

    private val images = mutableListOf<ImageUiModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            ImageItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bindItem(images[holder.adapterPosition], imageListener)
    }

    override fun getItemCount(): Int {
        return images.size
    }

    inner class ItemHolder(private val binding: ImageItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindItem(item: ImageUiModel, imageListener: ImageListener) {
            binding.ivImage.apply {
                load(item.image ?: "") { binding.pbImage.isVisible = false }
                setOnClickListener { imageListener.onClickImage(item, binding.ivImage) }
            }
        }
    }

    interface ImageListener {
        fun onClickImage(item: ImageUiModel, view: ImageView)
    }

    fun addAllPhotos(photos: List<ImageUiModel>) {
        this.images.apply {
            clear()
            addAll(photos)
        }
        notifyDataSetChanged()
    }

    fun resetPhotos() {
        images.clear()
        notifyDataSetChanged()
    }
}