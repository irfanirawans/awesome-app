package com.irfanirawansukirman.gallery.presentation.model

data class ImageUiModel(
    val id: Int?,
    val photographer: String?,
    var image: String?
)