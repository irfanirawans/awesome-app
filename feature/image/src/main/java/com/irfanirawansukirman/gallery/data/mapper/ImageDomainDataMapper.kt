package com.irfanirawansukirman.gallery.data.mapper

import com.irfanirawansukirman.core.Mapper
import com.irfanirawansukirman.gallery.data.model.ImageDataModel
import com.irfanirawansukirman.gallery.domain.entity.ImageDomainEntity
import javax.inject.Inject

class ImageDomainDataMapper @Inject constructor() : Mapper<ImageDataModel, ImageDomainEntity> {

    override fun before(previous: ImageDataModel?): ImageDomainEntity {
        return ImageDomainEntity(
            previous?.id,
            previous?.photographer,
            previous?.image
        )
    }

    override fun after(next: ImageDomainEntity?): ImageDataModel {
        return ImageDataModel(
            next?.id,
            next?.photographer,
            next?.image
        )
    }
}