package com.irfanirawansukirman.gallery

import android.os.SystemClock
import android.util.Log
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.irfanirawansukirman.gallery.presentation.ui.list.ImageActivity
import com.irfanirawansukirman.gallery.util.UnitTestHelper
import com.irfanirawansukirman.gallery.util.UnitTestHelper.isNotEmptyList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
@RunWith(AndroidJUnit4::class)
@LargeTest
class MovieTest {

    private var server: MockWebServer? = null

    @get:Rule
    val imageRule = ActivityTestRule(ImageActivity::class.java, false, true)

    @Before
    fun setupDepends() {
        startServer()
    }

    // ui test masih error. ada masalah dengan konfigurasi DI di class application
    @Test
    fun showImagesIsSuccess() {
        mockResponse(200, IMAGES_RESPONSE)

        SystemClock.sleep(2_000)

        onView(withId(R.id.rv_image)).check(matches(isNotEmptyList()))
    }

    @After
    fun clearDepends() {
        stopServer()
    }

    private fun startServer() {
        if (server == null) server = MockWebServer()
        server?.start(8080)
    }

    private fun stopServer() {
        server?.shutdown()
    }

    private fun mockResponse(
        code: Int = 200,
        responsePath: String,
        errorResponse: String = "HTTP ERROR 404"
    ) {
        try {
            server?.enqueue(
                getMockResponse(
                    code,
                    getResponseType(code, responsePath, errorResponse)
                )
            )
        } catch (e: Exception) {
            Log.e("ERROR ", e.message ?: "Error Bro")
        }
    }

    private fun getResponseType(
        code: Int,
        successResponse: String,
        errorResponse: String
    ): String {
        return if (code == 200) {
            UnitTestHelper.getStringFromFile(getTargetContext(), successResponse) ?: ""
        } else {
            errorResponse
        }
    }

    private fun getMockResponse(code: Int, response: String) =
        MockResponse().setResponseCode(code).setBody(response)

    private fun getTargetContext() = InstrumentationRegistry.getInstrumentation().targetContext

    companion object {
        private const val IMAGES_RESPONSE = "responses/images.json"
    }
}