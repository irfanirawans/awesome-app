package com.irfanirawansukirman.gallery.util

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher
import org.junit.Assert
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader


object UnitTestHelper {

    fun isNotEmptyList(): TypeSafeMatcher<View?>? {
        return object : TypeSafeMatcher<View?>() {
            override fun matchesSafely(item: View?): Boolean {
                if (item is RecyclerView) {
                    Assert.assertTrue(item.childCount > 0)
                    return true
                }
                return false
            }

            override fun describeTo(description: Description?) {
                description?.appendText("RecyclerView can not be empty");
            }
        }
    }

    @JvmStatic
    fun getStringFromFile(context: Context, filePath: String?): String? {
        val stream = context.classLoader.getResourceAsStream(filePath)
        var ret: String? = null
        try {
            ret = convertStreamToString(stream)
            // Make sure you close all streams.
            stream.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ret
    }

    @JvmStatic
    fun getStringFromFile(filePath: String?): String? {
        val stream = javaClass.classLoader?.getResource(filePath)?.openStream()
        var ret: String? = null
        try {
            ret = convertStreamToString(stream)
            // Make sure you close all streams.
            stream?.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return ret
    }

    @Throws(Exception::class)
    fun convertStreamToString(inputStream: InputStream?): String {
        val reader = BufferedReader(InputStreamReader(inputStream))
        val sb = StringBuilder()
        var line: String?
        while (reader.readLine().also { line = it } != null) {
            sb.append(line).append("\n")
        }
        reader.close()
        return sb.toString()
    }
}