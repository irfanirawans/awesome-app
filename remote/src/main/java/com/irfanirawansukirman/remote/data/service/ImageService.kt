package com.irfanirawansukirman.remote.data.service

import com.irfanirawansukirman.remote.data.response.ImageResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ImageService {

    @GET("search")
    suspend fun getImages(
        @Query("query") query: String = "random",
        @Query("per_page") perPage: Int = 10
    ): ImageResponse
}