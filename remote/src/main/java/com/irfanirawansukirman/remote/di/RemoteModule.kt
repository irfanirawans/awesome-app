package com.irfanirawansukirman.remote.di

import android.app.Application
import com.irfanirawansukirman.remote.BuildConfig
import com.irfanirawansukirman.remote.factory.ApiFactory
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class RemoteModule {

    @Singleton
    @Provides
    fun provideImageRetrofit(application: Application): Retrofit =
        ApiFactory.build(
            application,
            BuildConfig.PAXELS_BASE_URL,
            BuildConfig.PAXELS_API_KEY
        )
}