import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
}

val paxelsBaseUrl: String = gradleLocalProperties(rootDir).getProperty("PAXELS_BASE_URL")
val paxelsApiKey: String = gradleLocalProperties(rootDir).getProperty("PAXELS_API_KEY")

android {
    compileSdk = Android.compileSdk

    defaultConfig {
        minSdk = Android.minSdk
        targetSdk = Android.targetSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")

        buildConfigField("String", "PAXELS_BASE_URL", "\"" + paxelsBaseUrl + "\"")
        buildConfigField("String", "PAXELS_API_KEY", "\"" + paxelsApiKey + "\"")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}

dependencies {
    implementation(project(":core"))

    api(App.retrofit2)
    implementation(App.loggingInterceptor)
    implementation(App.okhttp)

    kapt(App.moshiKotlinCodegen)

    kapt(App.daggerCompiler)

    debugImplementation(App.chuck)
    releaseImplementation(App.chuckNoOp)
}