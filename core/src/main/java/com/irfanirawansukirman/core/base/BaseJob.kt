package com.irfanirawansukirman.core.base

interface BaseJob {

    fun initInjector() {}

    fun bindView() {}

    fun loadObserver() {}

    fun setupViewListener() {}
}