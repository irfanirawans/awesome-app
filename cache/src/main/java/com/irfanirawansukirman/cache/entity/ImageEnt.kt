package com.irfanirawansukirman.cache.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_image")
data class ImageEnt(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int?,

    @ColumnInfo(name = "photographer")
    val photographer: String?,

    @ColumnInfo(name = "image")
    val image: String?
)