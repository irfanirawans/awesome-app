package com.irfanirawansukirman.cache.dao

import androidx.room.Dao
import androidx.room.Query
import com.irfanirawansukirman.cache.base.BaseDao
import com.irfanirawansukirman.cache.entity.ImageEnt

@Dao
interface ImageDao : BaseDao<ImageEnt> {

    @Query("SELECT * FROM tb_image")
    suspend fun getAllImages(): List<ImageEnt>?
}