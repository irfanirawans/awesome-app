package com.irfanirawansukirman.cache.factory

import androidx.room.Database
import androidx.room.RoomDatabase
import com.irfanirawansukirman.cache.dao.ImageDao
import com.irfanirawansukirman.cache.entity.ImageEnt

@Database(
    entities = [ImageEnt::class],
    version = 1,
    exportSchema = false
)
abstract class CacheFactory : RoomDatabase() {

    abstract fun imageDao(): ImageDao
}