import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
}

val dbImage: String = gradleLocalProperties(rootDir).getProperty("DB_IMAGE")
val secretPreferenceName: String = gradleLocalProperties(rootDir).getProperty("SECRET_PREFERENCE_NAME")

android {
    compileSdk = Android.compileSdk

    defaultConfig {
        minSdk = Android.minSdk
        targetSdk = Android.targetSdk

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")

        buildConfigField("String", "DB_IMAGE", "\"" + dbImage + "\"")
        buildConfigField("String", "SECRET_PREFERENCE_NAME", "\"" + secretPreferenceName + "\"")
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }
}

dependencies {
    implementation(App.roomRuntime)
    kapt(App.roomCompiler)
    implementation(App.roomKtx)

    implementation(App.dagger)
    kapt(App.daggerCompiler)

    implementation(App.securityCrypto)

    testImplementation(App.junit)

    androidTestImplementation(App.roomTesting)
    androidTestImplementation(App.coreTesting)
    androidTestImplementation(App.espressoCore)
    androidTestImplementation(App.junitExt)
}