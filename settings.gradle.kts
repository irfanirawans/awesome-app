rootProject.name = "Awesome App"
include(":app")
include(":cache")
include(":remote")
include(":core")
include(":feature:image")